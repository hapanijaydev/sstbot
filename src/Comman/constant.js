import {Buffer} from 'buffer';
import {View} from 'react-native';
import {SafeAreaView, Dimensions} from 'react-native';
import CommanStyle from './CommanStyle';
import React from 'react';

// response code for check from bakand side
export const responseCodes = {
  OK: 200,
  CREATED: 201,
  NO_CONTENT: 204,
  BED_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  METHOD_NOT_ALLOWED: 405,
  REQUEST_TIME_OUT: 408,
  UNPROCESSED_ENTITY: 422,
  INTERNAL_SERVER_ERROR: 500,
  NOT_IMPLEMENTED: 501,
  BED_GATEWAY: 502,
  SERVICE_UNAVAILABLE: 503,
  GATE_WAY_TIMEOUT: 504,
  NETWORK_AUTH_REQUIRED: 511,
};

export const decodeBase64 = value => {
  return Buffer.from(value, 'base64').toString('ascii');
};

export const SafeAreaCustomView = ({
  Container,
  StatusBarColor,
  FullScreenColor,
}) => {
  return (
    <>
      <SafeAreaView style={{backgroundColor: StatusBarColor}} />
      <SafeAreaView
        style={[
          CommanStyle.SafeArea_Container,
          {backgroundColor: FullScreenColor},
        ]}>
        {Container && Container()}
      </SafeAreaView>
    </>
  );
};
export const Width = Dimensions.get('window').width;
export const Height = Dimensions.get('window').height;

//Normal
export const CHAT_ASSIST_PARAMS = ChatMessage => {
  return {
    model: 'gpt-3.5-turbo',
    prompt: ChatMessage,
    temperature: 0.9,
    max_tokens: 150,
    top_p: 1,
    frequency_penalty: 0.0,
    presence_penalty: 0.6,
    stop: ['Human:', 'AI:'],
  };
};

//For Images
export const IMAGE_ASSIST_PARAMS = (NumberOfImage, ImageSize, ChatMessage) => {
  return {
    prompt: ChatMessage,
    n: NumberOfImage,
    size: ImageSize,
  };
};

//GPT_3 modal
export const GPT_3_MODAL_CHAT_PARAMS = ChatMessage => {
  return {
    model: 'gpt-3.5-turbo',
    messages: [{role: 'user', content: ChatMessage}],
  };
};
