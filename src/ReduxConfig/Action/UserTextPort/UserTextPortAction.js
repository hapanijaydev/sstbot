import { USER_TEXT_PORT } from "../../ActionsList";

export function doUserText(params) {
  return async function (dispatch) {
    dispatch({type :  USER_TEXT_PORT,userTextPart : params});
  }
}

