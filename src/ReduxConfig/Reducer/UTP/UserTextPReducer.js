import {USER_TEXT_PORT} from '../../ActionsList';

const initialState = {
  text: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_TEXT_PORT:
      return {...state, text: action};
    default:
      return state;
  }
};
