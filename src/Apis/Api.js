// BASE URL
// export const BASE_URL = 'https://reqres.in/';

//Assess Auth
export const Auth = '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';

//ChatAssist
export const CHAT_ASSIST = 'https://api.openai.com/v1/completions';

//ImageAssist
export const IMAGE_ASSIST = 'https://api.openai.com/v1/images/generations';


//Chat AI ASSIST
export const GPT_3_CHAT_AI_ASSIST = 'https://api.openai.com/v1/chat/completions';
