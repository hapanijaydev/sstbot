export default {
    Welcome : 'Welcome to SSTbot',
    Thinker : 'Welcome to SSTbot - Swift Smarter Thinker.',
    Description1: 'SSTbot is an AI-based platform where one can get any information in the chat room window itself.',
    Description2: 'Get answers to all your questions in a simpler, swifter, and smarter manner just by typing your queries in the text area.',
    Start : 'Start',
    NetConnectivity : 'Please check your internet..',
    ErrorMessage : 'Please ask something..',
    ServerDown : 'SSTBot server down please try again after some time.',
    NumberOfImagesError : 'Maximum allows 10 number of images',
    NumberOfSizeQual : 'Size of numbers not same',
    NumberOfSizes : 'Maximum allows 1024 size of images',
    MaybeDataNotFound : 'May be data not found please try again'
}