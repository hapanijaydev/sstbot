import React, {useEffect} from 'react';
import { NativeModules } from 'react-native';
import {
  Animated,
  Dimensions,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  moderateScale,
  moderateVerticalScale,
  scale,
  verticalScale,
} from 'react-native-size-matters';
import image from '../../Assets/Image';
import NavigationService from '../../Navigation/NavigationService';
import {ScreenName} from '../../Navigation/ScreenName';
import strings from '../../themes/strings';

const Description = ({navigation}) => {
  //add top level navigation
  useEffect(() => {
    NavigationService.setTopLevelNavigator(navigation);
  }, []);

  //add button
  const OnPressButton = () => {
    navigation.navigate(ScreenName.Chat);
  };

  return (
    <SafeAreaView style={styles.Container}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Image
          source={image.Logo}
          style={{height: verticalScale(200), width: scale(200)}}
        />
        <Text
          style={{
            fontSize: moderateScale(18),
            fontWeight: '500',
            color: 'black',
            letterSpacing: moderateScale(0.5),
            marginTop: moderateScale(14),
            marginBottom: moderateScale(14),
          }}>
          {strings.Welcome}
        </Text>
        <Text style={styles.Commantext}>{strings.Description1}</Text>
        <Text style={styles.Commantext}>{strings.Description2}</Text>
        <TouchableOpacity
          style={{
            height: verticalScale(40),
            width: scale(100),
            borderRadius: moderateScale(4),
            marginTop: moderateScale(16),
            backgroundColor: '#00cbdd',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => OnPressButton()}>
          <Text
            style={{
              fontSize: moderateScale(14),
              color: 'white',
              fontWeight: '500',
              letterSpacing: moderateScale(0.5),
            }}>
            {strings.Start}
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default Description;

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: moderateVerticalScale(20),
  },
  Commantext: {
    fontSize: moderateScale(12),
    fontWeight: '500',
    color: 'gray',
    letterSpacing: moderateScale(0.5),
    marginTop: moderateScale(12),
    width: scale(300),
    textAlign: 'center',
  },
});
