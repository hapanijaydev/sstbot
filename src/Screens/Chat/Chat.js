import axios from 'axios';
import React, {useEffect, useRef, useState} from 'react';
import {
  Keyboard,
  ImageBackground,
  Image,
  Alert,
  Platform,
  Share,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import {ScrollView} from 'react-native';
import {KeyboardAvoidingView} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {TextInput, StyleSheet, Text, View} from 'react-native';
import {moderateScale, scale, verticalScale} from 'react-native-size-matters';
import image from '../../Assets/Image';
import LottieView from 'lottie-react-native';
import {
  SafeAreaCustomView,
  CHAT_ASSIST_PARAMS,
  IMAGE_ASSIST_PARAMS,
  GPT_3_MODAL_CHAT_PARAMS,
} from '../../Comman/constant';
import NetInfo from '@react-native-community/netinfo';
import strings from '../../themes/strings';
import colors from '../../themes/colors';
import Voice from './VoiceSopcher';
import ChatStyle from './ChatStyle';
import {CHAT_ASSIST, GPT_3_CHAT_AI_ASSIST, IMAGE_ASSIST} from '../../Apis/Api';
import UserMessageResponseView from '../../Components/UserResposeView/UserMessageResponseView';
import UserTextView from '../../Components/UserResposeView/UserTextView';
import SelectCatModal from '../../Components/Modals/SelectCatModal';

const Chat = () => {
  //UseStates
  const [loading, setLoading] = useState(false);
  const [ChatMessage, setChatMessage] = useState(undefined);
  const [obj, setObj] = useState([]);
  const [isNetwork, setIsNetWork] = useState(false);
  const [isPressInOut, setPressInOut] = useState(false);
  const [isImageKeyWord, setIsImageKeyWord] = useState(false);
  const [LoadMoreDataSave, setIsLoadMoreDataSave] = useState(undefined);
  const [CheckIsImageAvailable, setCheckIsImageAvailable] = useState(false);

  //CheckNetConnectivity && Voice
  useEffect(() => {
    getNetInfo();
    //All Voice Result
    Voice.onSpeechStart = e => {};
    Voice.onSpeechPartialResults = e => {};
    Voice.onSpeechRecognized = e => {};
    Voice.onSpeechResults = e => {
      SpeechResultAppend(e);
    };
    return () => {
      Voice.destroy().then(Voice.removeAllListeners);
    };
  }, []);

  // {"value": ["hello I am", "hello I am fine"]}  Voice Data
  //SpeechResultAppend
  const SpeechResultAppend = VoiceText => {
    let {value} = VoiceText;
    let TextPoint = '';
    value?.map((result, index) => {
      TextPoint = result;
    });
    // append data with write message
    setChatMessage(pres_state => pres_state + ' ' + TextPoint);
    //StartAgain
    StartAgainPort(); // when user stop speak
  };

  // /StartAgainPort
  const StartAgainPort = async () => {
    setSpeechError(false);
    setPressInOut(true);
    await Voice.onStartAgain('en-US', {
      RECOGNIZER_ENGINE: 'services',
      EXTRA_PARTIAL_RESULTS: true,
    });
  };

  //getNetInfo
  const getNetInfo = () => {
    NetInfo.addEventListener(state => {
      if (state.isConnected) {
        setIsNetWork(true);
      } else {
        setIsNetWork(false);
        Alert.alert('SSTBot', strings.NetConnectivity, [
          {text: 'OK', onPress: () => {}},
        ]);
      }
    });
  };

  //get Answer from OpenAI
  const getRes = (Url, Params) => {
    setLoading(true);
    axios({
      method: 'POST',
      url: Url,
      data: Params,
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'Bearer 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',
      },
    })
      .then(res => {
        if (res.status === 200) {
          //check data for image or chat
          let MessageData = res?.data?.data
            ? res?.data?.data
            : res?.data?.choices;
          console.log("MessageData => ", res?.data?.choices)
          setCheckIsImageAvailable(res?.data?.data ? true : false);
          //set Data in obj As a Object
          LetSetDataAsAObject(MessageData, CheckIsImageAvailable);
          setLoading(false);
        } else {
          Alert.alert('SSTBot', strings.ServerDown, [
            {text: 'OK', onPress: () => {}},
          ]);
          setLoading(false);
        }
        setChatMessage('');
      })
      .catch(e => {
        setChatMessage('');
        Alert.alert('SSTBot', strings.MaybeDataNotFound, [
          {text: 'OK', onPress: () => {}},
        ]);
        setLoading(false);
        console.log(e.message, e);
      });
  };

  //LetSetDataAsAObject
  const LetSetDataAsAObject = MessageData => {
    setObj(prevent => [
      ...prevent,
      {
        UserMessage: MessageData,
        isUserText: false,
        isSSTBotMessage: true,
      },
    ]);
  };

  //CallImageApi
  const CallImageApi = (IData, LasUserText) => {
    let {NumberOfImages, SizeOfImage1, SizeOfImage2} = IData;
    let ImageSizes = SizeOfImage1 + 'x' + SizeOfImage2;
    let ChatAddMessage = ChatMessage ? ChatMessage : LasUserText;
    //Note : = don't Pass directly NumberofImages other wise you will get error;
    let Params = IMAGE_ASSIST_PARAMS(
      Number(NumberOfImages),
      ImageSizes,
      ChatAddMessage,
    );
    console.log('IMAGE_ASSIST_PARAMS => ', Params);
    let URl = IMAGE_ASSIST;
    getRes(URl, Params); //For Chat_Assist
  };

  //LoadMoreImageData
  const LoadMoreImageData = () => {
    let LastUserText = '';
    obj &&
      obj?.map((item, index) => {
        if (item.UserText) {
          LastUserText = item.UserText;
        }
      });
    CallImageApi(LoadMoreDataSave, LastUserText);
  };

  //Send Message
  const OnSend = () => {
    let CheckUserEntries = ChatMessage.toLowerCase();
    // 'image' || 'images' || 'photo' || 'photos' || 'picture' || 'picturers'
    let get_image_flag_entries =
      CheckUserEntries.includes('image') ||
      CheckUserEntries.includes('images') ||
      CheckUserEntries.includes('photo') ||
      CheckUserEntries.includes('photos') ||
      CheckUserEntries.includes('picture') ||
      CheckUserEntries.includes('picturers');

    if (isNetwork) {
      if (ChatMessage != '') {
        //Add UserText
        //Check Image key word Flag
        if (get_image_flag_entries) {
          setIsImageKeyWord(true);
          setObj(prevent => [
            ...prevent,
            {UserText: ChatMessage, isUserText: true, isSSTBotMessage: false},
          ]);
        } else {
          setObj(prevent => [
            ...prevent,
            {UserText: ChatMessage, isUserText: true, isSSTBotMessage: false},
          ]);
          let Params = GPT_3_MODAL_CHAT_PARAMS(ChatMessage);
          let URl = GPT_3_CHAT_AI_ASSIST;
          getRes(URl, Params); //For Chat_Assist
        }
      } else {
        Alert.alert('SSTBot', strings.ErrorMessage, [
          {text: 'OK', onPress: () => {}},
        ]);
      }
    } else {
      Alert.alert('SSTBot', strings.NetConnectivity, [
        {text: 'OK', onPress: () => {}},
      ]);
    }
  };

  // // _OnPress
  const _OnPressMicroPhone = async () => {
    setSpeechError(false);
    setPressInOut(true);
    await Voice.start('en-US', {
      RECOGNIZER_ENGINE: 'services',
      EXTRA_PARTIAL_RESULTS: true,
    });
  };

  // // /_OnPressOutMicroPhone
  const _OnPressOutMicroPhone = async () => {
    setSpeechError(false);
    setPressInOut(false);
    await Voice.destroy();
  };

  //RefForScrollView
  const scrollViewRef = useRef();

  //onShare
  const onShare = async ShareMessagePort => {
    try {
      const result = await Share.share({
        message: ShareMessagePort,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      Alert.alert(error.message);
    }
  };

  return (
    <SafeAreaCustomView
      FullScreenColor={colors.colorWhite}
      StatusBarColor={colors.colorWhite}
      Container={() => {
        return (
          <View style={{flex: 1}}>
            <ImageBackground
              // source={image.backImage}
              resizeMode="contain"
              style={{flex: 1}}>
              <View style={ChatStyle.container}>
                <KeyboardAvoidingView
                  style={{flex: 1}}
                  behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
                  <ScrollView
                    automaticallyAdjustKeyboardInsets={true}
                    showsVerticalScrollIndicator={false}
                    ref={scrollViewRef}
                    onContentSizeChange={() => {
                      scrollViewRef.current.scrollToEnd({animated: true});
                    }}
                    style={ChatStyle.Scrollcontain}>
                    {obj && (
                      <FlatList
                        data={obj}
                        keyExtractor={(item, index) => index}
                        renderItem={({item, index}) => {
                          if (item?.isUserText)
                            return (
                              <UserTextView
                                key={index}
                                item={item}
                                index={index}
                              />
                            );
                          if (item?.isSSTBotMessage)
                            return (
                              <View>
                                <UserMessageResponseView
                                  key={index}
                                  UMessageData={item}
                                  ParentIndex={index}
                                  onShare={t => onShare(t)}
                                />
                              </View>
                            );
                        }}
                        ListFooterComponent={() => {
                          return (
                            !loading &&
                            CheckIsImageAvailable && (
                              <TouchableOpacity
                                style={ChatStyle.LoadMoreButton}
                                onPress={() => LoadMoreImageData()}>
                                <Text style={ChatStyle.LoadMoreText}>
                                  Load More
                                </Text>
                              </TouchableOpacity>
                            )
                          );
                        }}
                      />
                    )}
                    {loading && obj && (
                      <View style={ChatStyle.Loading}>
                        <LottieView
                          source={image.Lottie}
                          style={ChatStyle.LottieHight}
                          autoPlay
                        />
                      </View>
                    )}
                  </ScrollView>
                  <View style={ChatStyle.Container1}>
                    <View style={ChatStyle.container2}>
                      <TextInput
                        value={ChatMessage}
                        onChange={t => setChatMessage(t.nativeEvent.text)}
                        placeholder="Please enter your message"
                        autoCorrect={true}
                        multiline
                        style={ChatStyle.TextInputView}
                      />
                      <TouchableOpacity
                        onPressIn={() => _OnPressMicroPhone()}
                        onPressOut={() => _OnPressOutMicroPhone()}
                        style={ChatStyle.MicroPhoneView}>
                        {isPressInOut && (
                          <View
                            style={{
                              alignSelf: 'center',
                            }}>
                            <LottieView
                              source={image.LottieMicroPhone}
                              style={{height: verticalScale(50)}}
                              autoPlay
                            />
                          </View>
                        )}
                        {!isPressInOut && (
                          <Image
                            source={image.MicroPhone}
                            style={ChatStyle.MicroImage}
                            resizeMode="cover"
                          />
                        )}
                      </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                      onPress={() => {
                        Keyboard.dismiss();
                        OnSend();
                      }}
                      style={ChatStyle.SendButton}>
                      <Text style={ChatStyle.SendText}>Send</Text>
                    </TouchableOpacity>
                  </View>
                </KeyboardAvoidingView>
              </View>
            </ImageBackground>
            {isImageKeyWord && (
              <SelectCatModal
                IsVisible={isImageKeyWord}
                DataOfImages={data => {
                  setIsLoadMoreDataSave(data);
                  setIsImageKeyWord(false);
                  CallImageApi(data);
                }}
              />
            )}
          </View>
        );
      }}
    />
  );
};

export default Chat;

const styles = StyleSheet.create({});
