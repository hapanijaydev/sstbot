export default {
    backImage : require('./Images/backImage.png'),
    Logo : require('./Images/chat_gpt.png'),
    Lottie : require('../Assets/Lottie/loader.json'),
    LottieMicroPhone : require('../Assets/Lottie/MicroPhone.json'),
    MicroPhone : require('../Assets/Images/microphone.png'),
    ShareIcon : require('../Assets/Images/share.png'),
    Category : require('../Assets/Images/category.png'),
    ImagePort : require('../Assets/Images/Image.png'),
}