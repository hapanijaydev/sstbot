import {
  Alert,
  Modal,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {moderateScale, scale, verticalScale} from 'react-native-size-matters';
import colors from '../../themes/colors';
import strings from '../../themes/strings';

export default function SelectCatModal({IsVisible, DataOfImages}) {
  const [NumberOfImages, setNumberOfImages] = useState(5);
  const [SizeOfImage1, setSizeofImage1] = useState(512);
  const [SizeOfImage2, setSizeofImage2] = useState(512);

  //OkButton
  const OkButton = () => {
    if (NumberOfImages > 10) {
      Alert.alert('SSTBot', strings.NumberOfImagesError, [
        {text: 'OK', onPress: () => {}},
      ]);
    }else if (SizeOfImage1 > 1024 && SizeOfImage2 > 1024) {
      Alert.alert('SSTBot', strings.NumberOfSizes, [
        {text: 'OK', onPress: () => {}},
      ]);
    } else {
      DataOfImages &&
        DataOfImages({NumberOfImages, SizeOfImage1, SizeOfImage2});
    }
  };

  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        marginBottom: moderateScale(10),
      }}>
      <Modal animationType="slide" transparent={true} visible={IsVisible}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(52, 52, 52, 0.8)',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View style={Style.Container}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                height: verticalScale(30),
              }}>
              <Text>Number of Images : </Text>

              <TextInput
                value={NumberOfImages}
                defaultValue={'5'}
                onChange={e => setNumberOfImages(e.nativeEvent.text)}
                style={Style.Textinputttt}
                keyboardType="number-pad"
              />
            </View>
            <Text style={Style.TextContain}>Maximum 10</Text>
            <View style={Style.Container2}>
              <Text>Siz of Images : </Text>
              <TextInput
                value={SizeOfImage1}
                defaultValue={'512'}
                onChange={e => setSizeofImage1(e.nativeEvent.text)}
                style={Style.Textinputttt}
                keyboardType="number-pad"
              />
              <Text style={{marginLeft: moderateScale(10)}}>x</Text>
              <TextInput
                value={SizeOfImage2}
                defaultValue={'512'}
                onChange={e => setSizeofImage2(e.nativeEvent.text)}
                style={Style.Textinputttt}
                keyboardType="number-pad"
              />
            </View>
            <Text style={Style.TextContain}>Maximum 1024 x 1024</Text>
            <View
              style={{
                marginTop: moderateScale(6),
                flexDirection: 'row',
              }}>
              <TouchableOpacity style={Style.Button} onPress={() => OkButton()}>
                <Text style={Style.ButtonText}>Ok</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const Style = StyleSheet.create({
  ButtonText: {
    fontSize: moderateScale(12),
    color: colors.colorWhite,
    alignSelf: 'center',
  },

  Textinputttt: {
    height: verticalScale(30),
    width: scale(40),
    color: colors.colorBlack,
    fontSize: moderateScale(10),
    backgroundColor: colors.colorWhite,
    padding: moderateScale(2),
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: moderateScale(10),
    shadowColor: '#171717',
    shadowOffset: {width: 0, height: -0},
    shadowOpacity: Platform.OS == 'android' ? 0 : 0.2,
    shadowRadius: moderateScale(10),
    elevation: Platform.OS == 'android' ? 1 : 0,
  },
  Container: {
    minHeight: verticalScale(130),
    padding: moderateScale(8),
    minWidth: scale(200),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: moderateScale(8),
    backgroundColor: colors.colorWhite,
    shadowColor: '#171717',
    shadowOffset: {width: 0, height: -0},
    shadowOpacity: Platform.OS == 'android' ? 0 : 0.2,
    shadowRadius: moderateScale(10),
    elevation: Platform.OS == 'android' ? 1 : 0,
  },
  Container2: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: verticalScale(30),
    marginTop: moderateScale(10),
  },
  TextContain: {
    fontSize: moderateScale(8),
    alignSelf: 'flex-end',
    marginRight: moderateScale(20),
    marginTop: moderateScale(5),
  },
  Button: {
    height: verticalScale(30),
    width: scale(60),
    borderRadius: moderateScale(4),
    marginTop: moderateScale(10),
    backgroundColor: '#009caa',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
