import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import ChatStyle from '../../Screens/Chat/ChatStyle';

export default function UserTextView({item,index}) {
  return (
    <View key={index} style={ChatStyle.UserMessage}>
      <Text style={ChatStyle.YouText}>You :</Text>
      <Text style={ChatStyle.UserText}>{item.UserText}</Text>
    </View>
  );
}
