import {
  ActivityIndicator,
  Image,
  Linking,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import ChatStyle from '../../Screens/Chat/ChatStyle';
import {Height, Width} from '../../Comman/constant';
import Hyperlink from 'react-native-hyperlink';
import {moderateScale, scale, verticalScale} from 'react-native-size-matters';
import image from '../../Assets/Image';
import colors from '../../themes/colors';

export default function UserMessageResponseView({
  UMessageData,
  ParentIndex,
  onShare,
}) {
  //SetImageLoading Loader
  const [isImageLoading, setIsImageLoading] = useState(false);

  return (
    UMessageData?.UserMessage &&
    UMessageData?.UserMessage?.map((item, index) => {
      let ImageUrlPort = item?.url;
      let TextPort = item?.message?.content;
      let SharePart = ImageUrlPort ? ImageUrlPort : TextPort;
      // console.log('TextPort => ', TextPort.toArray());
      return (
        <View key={index} style={[ChatStyle.AlMessage]}>
          <View
            style={{
              height: verticalScale(30),
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              onPress={() => onShare && onShare(SharePart.trim())}>
              <Image
                source={image.ShareIcon}
                style={{
                  height: moderateScale(19),
                  width: moderateScale(18),
                  tintColor: '#83c5be',
                }}
              />
            </TouchableOpacity>
            <Text
              style={[
                ChatStyle.YouText,
                {
                  alignSelf: 'center',
                  marginBottom: 0,
                  marginLeft: moderateScale(10),
                },
              ]}>
              SSTBot :
            </Text>
          </View>
          {TextPort && (
            <Hyperlink onPress={url => (url ? Linking.openURL(url) : {})}>
              // <Text style={ChatStyle.UserText} >{TextPort.trim()}</Text>
            </Hyperlink>
          )}
          {ImageUrlPort && (
            <View
              style={{
                width: Width / 1.5,
                minHeight: Height / 3,
                resizeMode: 'stretch',
                justifyContent: 'center',
                borderRadius: moderateScale(8),
              }}>
              {isImageLoading && (
                <ActivityIndicator
                  size={'large'}
                  style={{alignSelf: 'center'}}
                  color={colors.colorWhite}
                />
              )}
              <Image
                source={{uri: ImageUrlPort}}
                onLoadStart={() => setIsImageLoading(true)}
                onLoadEnd={() => setIsImageLoading(false)}
                style={{
                  flex: isImageLoading ? 0 : 1,
                  resizeMode: 'stretch',
                  borderRadius: moderateScale(8),
                }}
              />
            </View>
          )}
        </View>
      );
    })
  );
}
