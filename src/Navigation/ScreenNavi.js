import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import { LogBox } from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import ScreenNavigation from './ScreenNavigation';

export default function ScreenNavi() {
    LogBox.ignoreAllLogs();
  return (
    <NavigationContainer>
      <ScreenNavigation />
    </NavigationContainer>
  );
}
