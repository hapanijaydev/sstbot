import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {ScreenName} from './ScreenName';
import Description from '../Screens/Description/Description';
import Chat from '../Screens/Chat/Chat';

const Stack = createStackNavigator();

export default function ScreenNavigation() {
  return (
    <Stack.Navigator
      initialRouteName={ScreenName.Description}
      screenOptions={{
        headerShown : false,
        gestureEnabled: false,
      }}>
      <Stack.Screen
        name={ScreenName.Description}
        component={Description}
      />
      <Stack.Screen
        name={ScreenName.Chat}
        component={Chat}
      />
    </Stack.Navigator>
  );
}
